// red_square.cpp


#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>
#include "draw.h"

using namespace std;

int main()
{
    sf::RenderWindow win(sf::VideoMode(WIDTH, HEIGHT), "Assignment 1");
    win.setFramerateLimit(60);

    // main animation loop
    while (win.isOpen()) {
        // check all window events that were triggered; always check for
        // sf::Event::Closed so that the window can be closed
        sf::Event event;
        while (win.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                win.close();
            }
        }

        // ... put drawing code here ...

        // end the current frame and display the window
        win.display();
    } // while
} // main
