// draw.h

#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>

// screen dimensions
const int WIDTH  = 500; // don't change these two lines:
const int HEIGHT = 500; // draw.cpp uses them

//
// You must implement process_lines. You can add your own
// helper functions if you like.
//

void process_lines(sf::RenderWindow& win) {
	sf::Vector2u dimensions = win.getSize(); // these lines are included
	std::cout << dimensions.x << " "         // to avoid a compiler error;
	          << dimensions.y << "\n";       // you should delete them when
	                                         // you're done

    // ... 
}
